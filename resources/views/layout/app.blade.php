<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Multi Bootstrap Template - Index</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ url('') }}asset/img/favicon.png" rel="icon">
  <link href="{{ url('') }}asset/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ url('asset/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ url('asset/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ url('asset/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Multi - v2.1.0
  * Template URL: https://bootstrapmade.com/multi-responsive-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html">Blue Developments</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{ route('welcome.student') }}">Home</a></li>
          <li><a href="{{ route('courses') }}">Courses</a></li>
          @if(!Auth::check())
          <li><a href="{{ route('register.student') }}">Register</a></li>
          <li><a href="{{ route('login') }}">Login</a></li>
          @else
          <li class="drop-down"><a href="">{{ Auth::user()->name }}</a>
            <ul>
              <li><a href="{{ route('student.logout') }}">Logout</a></li>
            </ul>
          </li>
          @endif
        </ul>
      </nav><!-- .nav-menu -->


    </div>
  </header><!-- End Header -->

  <main id="main">
<div style="margin-top: 90px;">
  	@if(session()->has('success'))
  		<div class="alert alert-success">
  			{{ session()->get('success') }}
  		</div>
  	@endif

  	@if(session()->has('error'))
  		<div class="alert alert-danger">
  			{{ session()->get('error') }}
  		</div>
  	@endif  	
</div>
  	@yield('content')

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span style="color: #ed502e;">Blue Development</span></strong>. All Rights Reserved
      </div>
      <div class="credits">

      </div>
    </div>

  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ url('asset/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ url('asset/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ url('asset/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ url('asset/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ url('asset/vendor/counterup/counterup.min.js') }}"></script>
  <script src="{{ url('asset/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ url('asset/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ url('asset/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ url('asset/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ url('asset/js/main.js') }}"></script>

</body>

</html>