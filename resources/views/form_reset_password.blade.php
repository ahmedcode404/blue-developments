@extends('layout.app')

@section('content')



    <div class="container" style="margin-top: 101px;">

    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <p>Reset Password Student In Blue Developments</p>
        </div>

        <div class="row content">
          <div class="col-lg-6 pt-4 pt-lg-0">


              <!-- form -->
              <div class="text-center">
                <form action="{{ route('reset.code') }}" method="post">

                  @csrf



                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Email</label>
                    </div>
                    <div class="col-lg-8">
                    <input type="text" name="email" class="form-control">
                        @if ($errors->has('email'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>




                  <button type="submit" class="btn btn-info">Reset Password</button>                    

                </form>
              </div>
              <!-- end form -->


          </div>
        </div>

      </div>
    </section><!-- End About Section -->


    </div>



@endsection