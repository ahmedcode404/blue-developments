@extends('layout.app')

@section('content')



    <div class="container" style="margin-top: 101px;">

    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <p>Register Student In Blue Developments</p>
        </div>

        <div class="row content">
          <div class="col-lg-6 pt-4 pt-lg-0">


              <!-- form -->
              <div class="text-center">
                <form action="{{ route('register.create') }}" method="post">

                  @csrf

                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Name</label>
                    </div>
                    <div class="col-lg-8">
                    <input type="text" name="name" class="form-control">
                        @if ($errors->has('name'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>


                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Email</label>
                    </div>
                    <div class="col-lg-8">
                    <input type="text" name="email" class="form-control">
                        @if ($errors->has('email'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>

                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Password</label>
                    </div>
                    <div class="col-lg-8">
                    <input type="password" name="password" class="form-control">
                        @if ($errors->has('password'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>

                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Confirm Password</label>
                    </div>
                    <div class="col-lg-8">
                    <input type="password" name="confirm_password" class="form-control">
                        @if ($errors->has('confirm_password'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('confirm_password') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>

                  <button type="submit" class="btn btn-info">Register</button>                    

                </form>
              </div>
              <!-- end form -->


          </div>
        </div>

      </div>
    </section><!-- End About Section -->


    </div>



@endsection