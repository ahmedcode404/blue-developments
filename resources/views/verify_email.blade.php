@extends('layout.app')

@section('content')



    <div class="container" style="margin-top: 101px;">

    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <p>Verify Email Student In Blue Developments</p>
        </div>

        <div class="row content">
          <div class="col-lg-6 pt-4 pt-lg-0">


              <!-- form -->
              <div class="text-center">
                <form action="{{ route('verify.email.active') }}" method="post">

                  @csrf



                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Code Verify Email</label>
                    </div>
                    <div class="col-lg-8">
                    <input type="text" name="verify_email" class="form-control">
                        @if ($errors->has('verify_email'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('verify_email') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>




                  <button type="submit" class="btn btn-info">Verify Email</button>                    

                </form>
              </div>
              <!-- end form -->


          </div>
        </div>

      </div>
    </section><!-- End About Section -->


    </div>



@endsection