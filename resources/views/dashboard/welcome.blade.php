@extends('dashboard.layout.app')

@section('content')





                <!--app-content open-->
				<div class="app-content">
					<div class="side-app">
						




						<!-- PAGE-HEADER -->

						<div class="page-header">
							<div>
								<h1 class="page-title">Dashboard</h1>
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="#">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
								</ol>
							</div>
							<div class="d-flex  ml-auto header-right-icons header-search-icon">
								<div class="dropdown d-sm-flex">
									<a href="#" class="nav-link icon" data-toggle="dropdown">
										<i class="fe fe-search"></i>
									</a>
									<div class="dropdown-menu header-search dropdown-menu-left">
										<div class="input-group w-100 p-2">
											<input type="text" class="form-control " placeholder="Search....">
											<div class="input-group-append ">
												<button type="button" class="btn btn-primary ">
													<i class="fa fa-search" aria-hidden="true"></i>
												</button>
											</div>
										</div>
									</div>
								</div><!-- SEARCH -->
								<div class="dropdown d-md-flex">
									<a class="nav-link icon full-screen-link nav-link-bg">
										<i class="fe fe-maximize fullscreen-button"></i>
									</a>
								</div><!-- FULL-SCREEN -->
								<div class="dropdown d-md-flex notifications">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fe fe-bell"></i>
										<span class="nav-unread badge badge-success badge-pill">2</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<div class="notifications-menu">
											<a class="dropdown-item d-flex pb-3" href="#">
												<div class="fs-16 text-success mr-3">
													<i class="fa fa-thumbs-o-up"></i>
												</div>
												<div class="">
													<strong>Someone likes our posts.</strong>
												</div>
											</a>
											<a class="dropdown-item d-flex pb-3" href="#">
												<div class="fs-16 text-primary mr-3">
													<i class="fa fa-commenting-o"></i>
												</div>
												<div class="">
													<strong>3 New Comments.</strong>
												</div>
											</a>
											<a class="dropdown-item d-flex pb-3" href="#">
												<div class="fs-16 text-danger mr-3">
													<i class="fa fa-cogs"></i>
												</div>
												<div class="">
													<strong>Server Rebooted</strong>
												</div>
											</a>
										</div>
										<div class="dropdown-divider"></div>
										<a href="#" class="dropdown-item text-center">View all Notification</a>
									</div>
								</div><!-- NOTIFICATIONS -->
								<div class="dropdown d-md-flex message">
									<a class="nav-link icon text-center" data-toggle="dropdown">
										<i class="fe fe-mail"></i>
										<span class="nav-unread badge badge-danger badge-pill">3</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<div class="message-menu">
											<a class="dropdown-item d-flex pb-3" href="#">
												<span class="avatar avatar-md brround mr-3 align-self-center cover-image" data-image-src="../assets/images/users/1.jpg"></span>
												<div>
													<strong>Madeleine</strong> Hey! there I' am available....
													<div class="small text-muted">
														3 hours ago
													</div>
												</div>
											</a>
											<a class="dropdown-item d-flex pb-3" href="#">
												<span class="avatar avatar-md brround mr-3 align-self-center cover-image" data-image-src="../assets/images/users/12.jpg"></span>
												<div>
													<strong>Anthony</strong> New product Launching...
													<div class="small text-muted">
														5 hour ago
													</div>
												</div>
											</a>
											<a class="dropdown-item d-flex pb-3" href="#">
												<span class="avatar avatar-md brround mr-3 align-self-center cover-image" data-image-src="../assets/images/users/4.jpg"></span>
												<div>
													<strong>Olivia</strong> New Schedule Realease......
													<div class="small text-muted">
														45 mintues ago
													</div>
												</div>
											</a>
											<a class="dropdown-item d-flex pb-3" href="#">
												<span class="avatar avatar-md brround mr-3 align-self-center cover-image" data-image-src="../assets/images/users/15.jpg"></span>
												<div>
													<strong>Sanderson</strong> New Schedule Realease......
													<div class="small text-muted">
														2 days ago
													</div>
												</div>
											</a>
										</div>
										<div class="dropdown-divider"></div>
										<a href="#" class="dropdown-item text-center">See all Messages</a>
									</div>
								</div><!-- MESSAGE-BOX -->
								<div class="dropdown profile-1">
									<a href="#" data-toggle="dropdown" class="nav-link pr-2 leading-none d-flex">
										<span>
											<img src="../assets/images/users/10.jpg" alt="profile-user" class="avatar  profile-user brround cover-image">
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<div class="drop-heading">
											<div class="text-center">
												<h5 class="text-dark mb-0">Elizabeth Dyer</h5>
												<small class="text-muted">Administrator</small>
											</div>
										</div>
										<div class="dropdown-divider m-0"></div>
										<a class="dropdown-item" href="#">
											<i class="dropdown-icon mdi mdi-account-outline"></i> Profile
										</a>
										<a class="dropdown-item" href="#">
											<i class="dropdown-icon  mdi mdi-settings"></i> Settings
										</a>
										<a class="dropdown-item" href="#">
											<span class="float-right"></span>
											<i class="dropdown-icon mdi  mdi-message-outline"></i> Inbox
										</a>
										<a class="dropdown-item" href="#">
											<i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">
											<i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?
										</a>
										<a class="dropdown-item" href="login.html">
											<i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
										</a>
									</div>
								</div>
								<div class="dropdown d-md-flex header-settings">
									<a href="#" class="nav-link icon " data-toggle="sidebar-right" data-target=".sidebar-right">
										<i class="fe fe-align-right"></i>
									</a>
								</div><!-- SIDE-MENU -->
							</div>
						</div>

						
						<!-- PAGE-HEADER END -->






						<!-- ROW-1 OPEN -->
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
								<div class="card bg-primary img-card box-primary-shadow">
									<div class="card-body">
										<div class="d-flex">
											<div class="text-white">
												<h2 class="mb-0 number-font">23,536</h2>
												<p class="text-white mb-0">Total Users </p>
											</div>
											<div class="ml-auto"> <i class="ion ion-person-add text-white fs-30 mr-2 mt-2"></i> </div>
										</div>
									</div>
								</div>
							</div><!-- COL END -->
							<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
								<div class="card bg-secondary img-card box-secondary-shadow">
									<div class="card-body">
										<div class="d-flex">
											<div class="text-white">
												<h2 class="mb-0 number-font">45,789</h2>
												<p class="text-white mb-0">Total Revenue</p>
											</div>
											<div class="ml-auto"> <i class="fa fa-bar-chart text-white fs-30 mr-2 mt-2"></i> </div>
										</div>
									</div>
								</div>
							</div><!-- COL END -->
							<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
								<div class="card  bg-success img-card box-success-shadow">
									<div class="card-body">
										<div class="d-flex">
											<div class="text-white">
												<h2 class="mb-0 number-font">89,786</h2>
												<p class="text-white mb-0">Total Profit</p>
											</div>
											<div class="ml-auto"> <i class="fa fa-dollar text-white fs-30 mr-2 mt-2"></i> </div>
										</div>
									</div>
								</div>
							</div><!-- COL END -->
							<div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
								<div class="card bg-info img-card box-info-shadow">
									<div class="card-body">
										<div class="d-flex">
											<div class="text-white">
												<h2 class="mb-0 number-font">43,336</h2>
												<p class="text-white mb-0">Monthly Sales</p>
											</div>
											<div class="ml-auto"> <i class="fa fa-cart-plus text-white fs-30 mr-2 mt-2"></i> </div>
										</div>
									</div>
								</div>
							</div><!-- COL END -->
						</div>
						<!-- ROW-1 CLOSED -->

						<div class="row">
							<div class="card">
								<div class="card-header border-bottom-0 p-4">
									<h2 class="card-title">1 - 30 of 546 </h2>
									<div class="page-options d-flex float-right">
										
									</div>
								</div>
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">All Products</h3>
									</div>
									<div class="btn-list">
									<a href="create-product.html" class="btn btn-primary"><i class="fe fe-plus mr-2"></i>Add New Products</a>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered text-nowrap w-100">
												<thead>
													<tr>
														<th class="wd-15p">ID</th>
														<th class="wd-15p">Product name</th>
														<th class="wd-10p">Date</th>
														<th class="wd-10p">Price</th>
														<th class="wd-10p">Seller</th>
														<th class="wd-15p">Status</th>
														<th class="wd-25p">Action</th>
														<th class="wd-10p">View</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Chloe</td>
														<td>2018/03/12</td>
														<td>654,765</td>
														<td>Koton</td>
														<td><div class="material-switch pull-left">
																	<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																	<label for="someSwitchOptionSuccess" class="label-success"></label>
																</div></td>
														<td><a class="btn btn-sm btn-primary" href="create-product.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.tr.playblackdesert.com%2FIntro%2FHashashin_Update%3FADcampaign%3Dhasashin%26ADSource%3Dfb%26ADTrackerName%3D200826%26ADMedium%3Dwca%26ADContent%3Dwca_pc%26fbclid%3DIwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw%23utm_campaign%3Dhasashin%26utm_source%3Dfb%26utm_term%3D200826%26utm_medium%3Dwca%26utm_content%3Dwca_pc&h=AT3o7zWckt_WcVTpBoVXa7Gr-u2fmD3plIF4LcV7VgvjlIQKrZuQ5a69xavD_1pYeC6Fxx1rvu-cWQ0AXGijWwVTiguFyGkmpOT8iQWjtAHpUX63KQGC0QMBNGUEx4Whkj3I&__tn__=*I&c[0]=AT31UECicZcaZAfDnzpeVLFJr7srqV8WBZkt9xyuyt86k75G-D7X5mwWCMGe3nd1YzWdE0qvOhulrfToriT023sf82SXkmHJPiV2iuphimA0XFpuYnOszPHeUoI4RR-M9vaxKeG6Zm-Jm4lxtSCmVOjqquTqoHec-3GnAU-3wxZA63HANTY6rPa5reJv1pr709lGZQEC1mBQzaVaEdM-"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>2</td>
														<td>Bond</td>
														<td>2012/02/21</td>
														<td>543,654</td>
														<td>Paul & Pear</td>
														<td><div class="material-switch pull-left">
																	<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																	<label for="someSwitchOptionSuccess" class="label-success"></label>
																</div></td>
														<td><a class="btn btn-sm btn-primary" href="create-product.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.tr.playblackdesert.com%2FIntro%2FHashashin_Update%3FADcampaign%3Dhasashin%26ADSource%3Dfb%26ADTrackerName%3D200826%26ADMedium%3Dwca%26ADContent%3Dwca_pc%26fbclid%3DIwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw%23utm_campaign%3Dhasashin%26utm_source%3Dfb%26utm_term%3D200826%26utm_medium%3Dwca%26utm_content%3Dwca_pc&h=AT3o7zWckt_WcVTpBoVXa7Gr-u2fmD3plIF4LcV7VgvjlIQKrZuQ5a69xavD_1pYeC6Fxx1rvu-cWQ0AXGijWwVTiguFyGkmpOT8iQWjtAHpUX63KQGC0QMBNGUEx4Whkj3I&__tn__=*I&c[0]=AT31UECicZcaZAfDnzpeVLFJr7srqV8WBZkt9xyuyt86k75G-D7X5mwWCMGe3nd1YzWdE0qvOhulrfToriT023sf82SXkmHJPiV2iuphimA0XFpuYnOszPHeUoI4RR-M9vaxKeG6Zm-Jm4lxtSCmVOjqquTqoHec-3GnAU-3wxZA63HANTY6rPa5reJv1pr709lGZQEC1mBQzaVaEdM-"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>3</td>
														<td>Carr</td>
														<td>20011/02/87</td>
														<td>86,000</td>
														<td>Bershka</td>
														<td><div class="material-switch pull-left">
																	<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																	<label for="someSwitchOptionSuccess" class="label-success"></label>
																</div></td>
														<td><a class="btn btn-sm btn-primary" href="create-product.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.tr.playblackdesert.com%2FIntro%2FHashashin_Update%3FADcampaign%3Dhasashin%26ADSource%3Dfb%26ADTrackerName%3D200826%26ADMedium%3Dwca%26ADContent%3Dwca_pc%26fbclid%3DIwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw%23utm_campaign%3Dhasashin%26utm_source%3Dfb%26utm_term%3D200826%26utm_medium%3Dwca%26utm_content%3Dwca_pc&h=AT3o7zWckt_WcVTpBoVXa7Gr-u2fmD3plIF4LcV7VgvjlIQKrZuQ5a69xavD_1pYeC6Fxx1rvu-cWQ0AXGijWwVTiguFyGkmpOT8iQWjtAHpUX63KQGC0QMBNGUEx4Whkj3I&__tn__=*I&c[0]=AT31UECicZcaZAfDnzpeVLFJr7srqV8WBZkt9xyuyt86k75G-D7X5mwWCMGe3nd1YzWdE0qvOhulrfToriT023sf82SXkmHJPiV2iuphimA0XFpuYnOszPHeUoI4RR-M9vaxKeG6Zm-Jm4lxtSCmVOjqquTqoHec-3GnAU-3wxZA63HANTY6rPa5reJv1pr709lGZQEC1mBQzaVaEdM-"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>4</td>
														<td>Dyer</td>
														<td>2014/08/23</td>
														<td>456,123</td>
														<td>GAP</td>
														<td><div class="material-switch pull-left">
																	<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																	<label for="someSwitchOptionSuccess" class="label-success"></label>
																</div></td>
														<td><a class="btn btn-sm btn-primary" href="create-product.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.tr.playblackdesert.com%2FIntro%2FHashashin_Update%3FADcampaign%3Dhasashin%26ADSource%3Dfb%26ADTrackerName%3D200826%26ADMedium%3Dwca%26ADContent%3Dwca_pc%26fbclid%3DIwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw%23utm_campaign%3Dhasashin%26utm_source%3Dfb%26utm_term%3D200826%26utm_medium%3Dwca%26utm_content%3Dwca_pc&h=AT3o7zWckt_WcVTpBoVXa7Gr-u2fmD3plIF4LcV7VgvjlIQKrZuQ5a69xavD_1pYeC6Fxx1rvu-cWQ0AXGijWwVTiguFyGkmpOT8iQWjtAHpUX63KQGC0QMBNGUEx4Whkj3I&__tn__=*I&c[0]=AT31UECicZcaZAfDnzpeVLFJr7srqV8WBZkt9xyuyt86k75G-D7X5mwWCMGe3nd1YzWdE0qvOhulrfToriT023sf82SXkmHJPiV2iuphimA0XFpuYnOszPHeUoI4RR-M9vaxKeG6Zm-Jm4lxtSCmVOjqquTqoHec-3GnAU-3wxZA63HANTY6rPa5reJv1pr709lGZQEC1mBQzaVaEdM-"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>5</td>
														<td>Hill</td>
														<td>2010/7/14</td>
														<td>432,230</td>
														<td>Polo</td>
														<td><div class="material-switch pull-left">
																	<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																	<label for="someSwitchOptionSuccess" class="label-success"></label>
																</div></td>
														<td><a class="btn btn-sm btn-primary" href="create-product.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.tr.playblackdesert.com%2FIntro%2FHashashin_Update%3FADcampaign%3Dhasashin%26ADSource%3Dfb%26ADTrackerName%3D200826%26ADMedium%3Dwca%26ADContent%3Dwca_pc%26fbclid%3DIwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw%23utm_campaign%3Dhasashin%26utm_source%3Dfb%26utm_term%3D200826%26utm_medium%3Dwca%26utm_content%3Dwca_pc&h=AT3o7zWckt_WcVTpBoVXa7Gr-u2fmD3plIF4LcV7VgvjlIQKrZuQ5a69xavD_1pYeC6Fxx1rvu-cWQ0AXGijWwVTiguFyGkmpOT8iQWjtAHpUX63KQGC0QMBNGUEx4Whkj3I&__tn__=*I&c[0]=AT31UECicZcaZAfDnzpeVLFJr7srqV8WBZkt9xyuyt86k75G-D7X5mwWCMGe3nd1YzWdE0qvOhulrfToriT023sf82SXkmHJPiV2iuphimA0XFpuYnOszPHeUoI4RR-M9vaxKeG6Zm-Jm4lxtSCmVOjqquTqoHec-3GnAU-3wxZA63HANTY6rPa5reJv1pr709lGZQEC1mBQzaVaEdM-"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													
												</tbody>
											</table>
										</div>
									</div>
									<!-- TABLE WRAPPER -->
								</div>
								<!-- SECTION WRAPPER -->
							</div>
						</div>
						<!-- ROW-1 CLOSED -->


						<div class="row">
							<div class="card">
								<div class="card-header border-bottom-0 p-4">
									<h2 class="card-title">1 - 30 of 546 </h2>
									<div class="page-options d-flex float-right">
										
									</div>
								</div>
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Active orders</h3>
									</div>
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered text-nowrap w-100">
												<thead>
													<tr>
														<th class="wd-15p">ID</th>
														<th class="wd-15p">Product name</th>
														<th class="wd-10p">Date</th>
														<th class="wd-10p">Price</th>
														<th class="wd-10p">Seller</th>
														<th class="wd-15p">Buyer</th>
														<th class="wd-25p">Action</th>
														<th class="wd-10p">View</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Chloe</td>
														<td>2018/03/12</td>
														<td>$654,765</td>
														<td>Koton</td>
														<td>Buyer Name</td>
														<td><a class="btn btn-sm btn-primary" href="edit-order.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="profile.html"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>2</td>
														<td>Bond</td>
														<td>2012/02/21</td>
														<td>$543,654</td>
														<td>Paul & Pear</td>
														<td>Buyer Name</td>
														<td><a class="btn btn-sm btn-primary" href="edit-order.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="profile.html"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>3</td>
														<td>Carr</td>
														<td>20011/02/87</td>
														<td>$86,000</td>
														<td>Bershka</td>
														<td>Buyer Name</td>
														<td><a class="btn btn-sm btn-primary" href="edit-order.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="profile.html"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>4</td>
														<td>Dyer</td>
														<td>2014/08/23</td>
														<td>$456,123</td>
														<td>GAP</td>
														<td>Buyer Name</td>
														<td><a class="btn btn-sm btn-primary" href="edit-order.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="profile.html"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													<tr>
														<td>5</td>
														<td>Hill</td>
														<td>2010/7/14</td>
														<td>$432,230</td>
														<td>Polo</td>
														<td>Buyer Name</td>
														<td><a class="btn btn-sm btn-primary" href="edit-order.html"><i class="fa fa-edit"></i> Edit</a>
															<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
														</td>
														<td><a class="btn btn-sm btn-secondary" href="profile.html"><i class="fa fa-info-circle"></i> View</a> </td>
													</tr>
													
												</tbody>
											</table>
										</div>
									</div>
									<!-- TABLE WRAPPER -->
								</div>
								<!-- SECTION WRAPPER -->
							</div>
						</div>
						<!-- ROW-1 CLOSED -->

							<!-- ROW-1 OPEN -->
							<div class="row">
								<div class="card">
									<div class="card-header border-bottom-0 p-4">
										<h2 class="card-title">1 - 30 of 546 </h2>
										
									</div>
								<div class="col-md-12 col-lg-12">
									<div class="card">
										<div class="card-header">
											<h3 class="card-title">All Usersr</h3>
										</div>
										<div class="page-options d-flex float-right">
											<div class="btn-list">
												<a href="send-email.html"><button" class="btn btn-primary"><i class="fe fe-plus mr-2"></i>Send Email</button></a>
											</div>
										</div>
										
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered text-nowrap w-100">
													<thead>
														<tr>
															<th class="wd-15p"><input type="checkbox" /></th>
															<th class="wd-15p">Name</th>
															<th class="wd-15p">Start date</th>
															<th class="wd-25p">E-mail</th>
															<th class="wd-10p">Sales</th>
															<th class="wd-10p">Purchases</th>
															<th class="wd=10p">Status</th>
															<th class="wd-15p">Options</th>
															<th class="wd-10p">Info</th>

														</tr>
													</thead>
													<tbody>
														
														<tr>
															<td><input type="checkbox" /></td>
															<td>Nash</td>
															<td>2015/05/03</td>
															<td>b.nash@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>

														</tr>
														<tr>
															<td><input type="checkbox" /></td>
															<td>Yamamoto</td>
															<td>2010/08/19</td>
															<td>s.yamamoto@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>
														</tr>
														<tr>
															<td><input type="checkbox" /></td>
															<td>Walton</td>
															<td>2012/08/11</td>
															<td>t.walton@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>
														</tr>
														<tr>
															<td><input type="checkbox" /></td>
															<td>Camacho</td>
															<td>2016/07/07</td>
															<td>f.camacho@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>
														</tr>
														<tr>
															<td><input type="checkbox" /></td>
															<td>Baldwin</td>
															<td>2017/04/09</td>
															<td>s.baldwin@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>
														</tr>
														<tr>
															<td><input type="checkbox" /></td>
															<td>Frank</td>
															<td>2018/01/04</td>
															<td>z.frank@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>
														</tr>
														<tr>
															<td><input type="checkbox" /></td>
															<td>Serrano</td>
															<td>2017/06/01</td>
															<td>z.serrano@datatables.net</td>
															<td>350</td>
															<td>100</td>
															<td><div class="material-switch pull-left">
																<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
																<label for="someSwitchOptionSuccess" class="label-success"></label>
															</div></td>
															<td><a class="btn btn-sm btn-primary" href="edit-user.html"><i class="fa fa-edit"></i> Edit</a>
																<a class="btn btn-sm btn-danger" href="#"><i class="fa fa-trash"></i> Delete</a>
															</td>
															<td><a class="btn btn-sm btn-secondary" href="https://sample.foxytech.net/5/76/my-account.html?fbclid=IwAR1Hh4VpqVu-djPlFKreEhOveqT2Hz5SCiTHCoFzAi6z-hS4qWUDTW-vyvw"><i class="fa fa-info-circle"></i> Details</a> </td>
														</tr>
														
													</tbody>
												</table>
											</div>
										</div>
										<!-- TABLE WRAPPER -->
									</div>
									<!-- SECTION WRAPPER -->
								</div>
							</div>
							<!-- ROW-1 CLOSED -->
					</div>
				</div>
				<!-- CONTAINER CLOSED -->
			</div>


			

		</div>
	</div>


@endsection