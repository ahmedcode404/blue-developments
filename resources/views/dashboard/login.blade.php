<!DOCTYPE html>
<html lang="en" dir="rtl">

<!-- Mirrored from demo.interface.club/limitless/demo/bs3/Template/layout_1/RTL/default/full/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Mar 2019 08:59:09 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
    </title>

<style type="text/css">
    .login-form{
        text-align: center;
    }
    input{
        margin: 5px;
    }
</style>


</head>

<body class="login-container">

    @if(session()->has('error'))
        <div style="color: red;text-align: center;">
            {{ session()->get('error') }}
        </div>
    @endif
                        @if (session('success'))
                            <div style="color: green;text-align: center;">
                                {{ session('success') }}
                            </div>
                        @endif    
        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">

                        <!-- Simple login form -->
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="panel panel-body login-form" >
                                <div class="text-center">
                                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                                    <h2 class="content-group">Login </h2>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ trans('app.Email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ trans('app.Password') }}" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Login <i class="icon-circle-left2 position-right"></i></button>
                                </div>

                            </div>
                        </form>
                        <!-- /simple login form -->
                        <!-- /simple login form -->



                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->




</body>

</html>
