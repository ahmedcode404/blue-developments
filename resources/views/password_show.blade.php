@extends('layout.app')

@section('content')



    <div class="container" style="margin-top: 101px;">

    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <p>Update Password Student In Blue Developments</p>
        </div>

        <div class="row content">
          <div class="col-lg-6 pt-4 pt-lg-0">


              <!-- form -->
              <div class="text-center">
                <form action="{{ route('update.password' , $code_reset) }}" method="post">

                  @csrf

                  <input type="hidden" name="code_reset" value="{{ $code_reset }}">

                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Password </label>
                    </div>
                    <div class="col-lg-8">
                    <input type="password" name="password" class="form-control">
                        @if ($errors->has('email'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>

                  <div class="form-group col-lg-12">
                    <div class="row">
                    <div class="col-lg-4">
                    <label>Confirm Password </label>
                    </div>
                    <div class="col-lg-8">
                    <input type="password" name="confirm_password" class="form-control">
                        @if ($errors->has('email'))
                            <span style="color: red;">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif                    
                    </div>
                  </div>
                  </div>                  




                  <button type="submit" class="btn btn-info">Update Password</button>                    

                </form>
              </div>
              <!-- end form -->


          </div>
        </div>

      </div>
    </section><!-- End About Section -->


    </div>



@endsection