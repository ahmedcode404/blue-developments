@extends('layout.app')

@section('content')


    <!-- ======= courses Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <p>Courses Blue Developments</p>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">


          @foreach($courses as $course)
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="{{ url('asset/img/courses/' . $course->image) }}" width="500px" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>{{ $course->name }}</h4>

              <form action="{{ route('enroll') }}" method="post">
                @csrf

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                <input type="hidden" name="course_id" value="{{ $course->id }}">

                <button type="submit" class="details-link" title="More Details">Enroll</button>
  
              </form>

            </div>
          </div>
          @endforeach


        </div>

      </div>
    </section><!-- End courses Section -->


@endsection

