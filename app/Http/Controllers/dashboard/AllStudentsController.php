<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
class AllStudentsController extends Controller
{
    public function index ()
    {
    	$students = User::all();

    	return view('dashboard.students.index' , compact('students'));
    }

    public function view (User $student)
    {
        return view('dashboard.students.view' , compact('student'));
    }    

    public function delete ($student)
    {
    	$student = User::find($student);

    	$student->delete();

    	return redirect()->back()->with('success' , 'Deleted Is Successfully');
    }    
}
