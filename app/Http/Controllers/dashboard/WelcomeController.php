<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class WelcomeController extends Controller
{
    public function login()
    {
    	return view('dashboard.login');
    }

    public function loginAuth(Request $request)
    {
    	$request->validate([
    		'email' => 'required|email',
    		'password' => 'required|min:6|max:12',
    	]);

    	if (Auth::guard('admin')->attempt(request(['email' , 'password'])) == false) {
    		return redirect()->back()->with('error' , 'email or password iscorrect');
    	}
    	return redirect('dashboard')->with('success' , 'Login Student Is Successfully');    	
    }    

    public function welcome()
    {
    	return view('dashboard.welcome');
    }

    public function logout()
    {
    	Auth::guard('admin')->logout();

    	return redirect()->route('login.admin');    	
    	
    } 

}
