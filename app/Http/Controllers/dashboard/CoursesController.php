<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Course;
class CoursesController extends Controller
{
    public function index()
    {
    	$courses = Course::all();

    	return view('dashboard.courses.index' , compact('courses'));
    }

    public function create()
    {
    	return view('dashboard.courses.create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ]);

        //$request_data = $request->all()

        if ($file = $request->file('image')) {
            
            $destenation_path = public_path('/asset/img/courses/');
            $name_image = uniqid() . $file->getClientOriginalName();
            $file->move($destenation_path , $name_image);


        }

        $course = new Course();
        $course->name = $request->name;
        $course->image = $name_image;
        $course->save();

 

        return redirect()->route('dashboard.courses.index')->with('success','Added Courses Successfuly');

    }

	public function edit(Course $course)
	{
		return view('dashboard.courses.edit' , compact('course'));
	}

    public function update(Request $request , Course $course)
    {

        $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ]);

        //$request_data = $request->all()



        //$course = Course::find($course);
        $course->name = $request->name;

        if ($file = $request->file('image')) {
            
            $destenation_path = public_path('/asset/img/courses/');
            $name_image = uniqid() . $file->getClientOriginalName();
            $file->move($destenation_path , $name_image);

        	$course->image = $name_image;

        }    

        $course->update();

 

        return redirect()->route('dashboard.courses.index')->with('success','Updated Courses Successfuly');

    }

    public function destroy (Course $course)
    {

    	$course->delete();

    	return redirect()->back()->with('success' , 'Deleted Is Successfully');
    }     	           
}
