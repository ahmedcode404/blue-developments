<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\User;
use Hash;
class AddAdminAndStudentController extends Controller
{
    public function add()
    {
    	return view('dashboard.add.create');
    }


    public function addAdminOrStudent(Request $request)
    {
    	if ($request->role == 'admin') {
    		

    	$request->validate([
    		'name' => 'required|string',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|min:6|max:12',
    		'confirm_password' => 'required|min:6|max:12:same:password',
    		'role' => 'required',
    	]);

    	$user = new Admin();
    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
    	$user->password = Hash::make($request->get('password'));
    	$user->save();


    	return redirect()->route('dashboard.admins')->with('success' , 'Create Admin Is Successfully');


    	} else {


    	$request->validate([
    		'name' => 'required|string',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|min:6|max:12',
    		'confirm_password' => 'required|min:6|max:12:same:password',
    		'role' => 'required',
    	]);

    	$user = new User();
    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
    	$user->password = Hash::make($request->get('password'));
    	$user->save();


    	return redirect()->route('students')->with('success' , 'Create Student Is Successfully');


    	} // end else function role 

    }

     

}
