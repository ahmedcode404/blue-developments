<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
class AllAdminsController extends Controller
{
    public function admins()
    {
    	$admins = Admin::all();
    	return view('dashboard.admins.index' , compact('admins'));
    }

    public function delete ($admin)
    {
    	$student = Admin::find($admin);

    	$student->delete();

    	return redirect()->back()->with('success' , 'Deleted Is Successfully');
    }


}
