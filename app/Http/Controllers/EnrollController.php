<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class EnrollController extends Controller
{
    public function enroll(Request $request)
    {
    	$student = User::find($request->user_id);

    	$student->courses()->attach($request->course_id);

    	return redirect()->back()->with('success' , 'Enroll Is Successfully');

    }
}
