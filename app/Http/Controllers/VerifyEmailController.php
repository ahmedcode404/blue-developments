<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class VerifyEmailController extends Controller
{
    public function verify_email($verify_email)
    {
    	return view('verify_email' , compact('verify_email'));
    }

    public function verify_email_active(Request $request)
    {
    	$student = User::where('code_verify_email' , $request->verify_email)->first();
	    if ( !$student ) return redirect()->back()->with('error' , 'Code Verify Email Not Found');

    	 $student->email_verified_at = 'Active';

    	 $student->update();

    	auth()->login($student);

    	return redirect()->route('welcome.student')->with('success' , 'Verify Email Is Successfully');

    }
}
