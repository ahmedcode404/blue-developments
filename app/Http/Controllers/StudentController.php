<?php

namespace App\Http\Controllers;
use App\Mail\VerifyEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\User;
use Hash;
class StudentController extends Controller
{
    public function register()
    {
    	return view('register');
    }

    public function register_create(Request $request)
    {
    	$request->validate([
    		'name' => 'required|string',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|min:6|max:12',
    		'confirm_password' => 'required|min:6|max:12:same:password',
    	]);

    	$user = new User();
    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
    	$user->code_verify_email = rand() . 9999;

    	$user->save();

        $verify_email = $user->code_verify_email;
        
        Mail::to($user->email)->send(new VerifyEmail($verify_email));

    	//auth()->login($user);

    	return redirect()->route('welcome.student')->with('success' , 'Please check Email For Verify');
    }

    public function login()
    {
        return view('login');
    }

    public function login_student(Request $request)
    {
    	$request->validate([
    		'email' => 'required|email',
    		'password' => 'required|min:6|max:12',
    	]);

    	if (auth()->attempt(request(['email' , 'password'])) == false) {
    		return redirect()->back()->with('error' , 'email or password iscorrect');
    	}
    	return redirect()->route('welcome.student')->with('success' , 'Login Student Is Successfully');    	
    }


    public function logout()
    {
    	auth()->logout();

    	return view('login');
    }        
}
