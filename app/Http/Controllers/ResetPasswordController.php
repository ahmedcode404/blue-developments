<?php

namespace App\Http\Controllers;
use App\Mail\ResetPassword;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use App\User;
use Hash;
class ResetPasswordController extends Controller
{
    public function index()
    {
    	return view('form_reset_password');
    }


	public function sendPasswordResetToken(Request $request)
	{
	    $student = User::where('email', $request->email)->first();
	    if ( !$student ) return redirect()->back()->with('error' , 'Email Not Found');

	    $code = rand() . 6666;
	    //create a new token to be sent to the user. 
	    $code_reset = $student->code_reset_password = $code;
	    $student->update();

	    Mail::to($request->email)->send(new ResetPassword($code_reset));

    	return redirect()->back()->with('success' , 'check Email For Reset Password');	    


	}

	public function reset_code_password($code_reset)
	{
	    $student = User::where('code_reset_password', $code_reset)->first();

	    if ( !$student ) return redirect()->route('welcome.student')->with('error' , 'Reset Code Not Found !');

	    return view('password_show' , compact('code_reset'));

	}

	public function update_password(Request $request , $code_reset)
	{

		$request->validate([
			'password' => 'required',
			'confirm_password' => 'required|same:password',
		]);

	 $student = User::where('code_reset_password', $code_reset)->first();
	    if ( !$student ) return redirect()->route('welcome.student')->with('error' , 'Reset Code Not Found !');

     $student->password = Hash::make($request->password);
     $student->update(); //or $user->save();

     auth()->login($student);

    	return redirect()->route('welcome.student')->with('success' , 'Login Is Successfully');	    

	}    
}
