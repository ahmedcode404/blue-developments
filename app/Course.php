<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name' , 'image'];

    public function students()
    {
    	return $this->belongsToMany(Student::class , 'student_courses');
    }
}
