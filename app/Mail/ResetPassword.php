<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $code_reset;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code_reset)
    {
        $this->code_reset = $code_reset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('ahmedcode314@gmail.com')->markdown('emails.contact.reset-password');
    }
}
