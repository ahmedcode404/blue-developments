<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $verify_email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($verify_email)
    {
        $this->verify_email = $verify_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('ahmedcode314@gmail.com')->markdown('emails.contact.verify-email');
    }
}
