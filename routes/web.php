<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// route welcome
Route::get('/' , 'WelcomeController@welcome')->name('welcome.student');
// route student register
Route::get('register' , 'StudentController@register')->name('register.student');
Route::post('register/create' , 'StudentController@register_create')->name('register.create');
// route student login
Route::get('login' , 'StudentController@login')->name('login');
Route::post('login/student' , 'StudentController@login_student')->name('login.student');
// route student logout
Route::get('logout' , 'StudentController@logout')->name('student.logout');

// reset password
Route::get('reset/password' , 'ResetPasswordController@index')->name('reset.password');
Route::post('reset/code' , 'ResetPasswordController@sendPasswordResetToken')->name('reset.code');
Route::get('reset/code/password/{code_reset}' , 'ResetPasswordController@reset_code_password')->name('reset.code.password');
Route::post('update/password/{code_reset}' , 'ResetPasswordController@update_password')->name('update.password');

Route::get('verify/email/{verify_email}' , 'VerifyEmailController@verify_email')->name('verify.email');

Route::post('verify/email/active' , 'VerifyEmailController@verify_email_active')->name('verify.email.active');


Route::group(['middleware' => 'auth'] , function () {

	Route::get('courses' , 'CoursesController@index')->name('courses');
	Route::post('enroll' , 'EnrollController@enroll')->name('enroll');

});


