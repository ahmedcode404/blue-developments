<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login/admin' , 'WelcomeController@login')->name('login.admin');
Route::post('login' , 'WelcomeController@loginAuth')->name('login');
Route::get('logout/admin' , 'WelcomeController@logout')->name('logout.admin');

Route::prefix('dashboard')->name('dashboard.')->middleware('auth:admin')->group(function () {

	Route::get('/' , 'WelcomeController@welcome');

	// route categories
	Route::resource('courses' , 'CoursesController');
	// route ernolls
	Route::get('ernolls' , 'ErnollController@index')->name('ernolls');
	// route admins
	Route::get('admins' , 'AllAdminsController@admins')->name('admins');
	Route::delete('admin/{admin}' , 'AllAdminsController@delete')->name('admin.delete');

	// route users
	Route::get('students' , 'AllStudentsController@index')->name('students');
	Route::delete('student/{student}' , 'AllStudentsController@delete')->name('student.delete');
	Route::get('student/view/{student}' , 'AllStudentsController@view')->name('student.view');
	// route add admin or student categories
	Route::get('add' , 'AddAdminAndStudentController@add')->name('add');
	Route::post('addAdminOrStudent' , 'AddAdminAndStudentController@addAdminOrStudent')->name('addAdminOrStudent');


});


?>